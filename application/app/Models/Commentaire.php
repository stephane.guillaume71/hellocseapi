<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Commentaire extends Model
{
    use HasFactory;
    use HasUuids;

    protected $fillable = [
        'content',
        'profil_id',
        'administrateur_id'
    ];

    public function profil(): BelongsTo
    {
        return $this->belongsTo(Profil::class);
    }

}
