<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Profil extends Model
{
    use HasFactory;
    use HasUuids;

    protected $fillable = [
        'firstname',
        'lastname',
        'picture',
        'status',
        'administrateur_id'
    ];

    public function commentaires(): HasMany
    {
        return $this->hasMany(Commentaire::class);
    }
}
