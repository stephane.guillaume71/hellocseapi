<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfilRequest;
use App\Models\Profil;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;


class ProfilController extends Controller
{

    /**
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json([
            'success' => false,
            'code' => 200,
            'message' => "Profil - La liste de tous les profils",
            'data' => Profil::all()
        ]);
    }

    /**
     * @param Request $request
     */
    public function allActif(Request $request)
    {
        if ($request->bearerToken()) {
            return redirect('/api/profils/all');
        }
        try {
            $profils = Profil::select('firstname', 'lastname', 'picture')->where('status', 'ACTIF')->get();

            return response()->json([
                'success' => false,
                'code' => 200,
                'message' => "Profil - La liste de tous les profils Actif",
                'data' => $profils
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'code' => 422,
                'message' => "Profil - Erreur lors de la récupération de tous les profils Actif",
            ]);
        }
    }

    /**
     * @param Profil $profil
     * @return JsonResponse
     */
    public function show(Profil $profil): JsonResponse
    {
        return response()->json([
            'success' => false,
            'code' => 200,
            'message' => "Profil - Un seul Profil",
            'data' => $profil
        ]);
    }

    /**
     * Create profil
     *
     * @param ProfilRequest $request
     * @return JsonResponse
     */
    public function store(ProfilRequest $request): JsonResponse
    {
        $code = 200;
        $response = [
            'code' => $code,
            'success' => true,
            'message' => "Profil - l'enregistrement du profil a bien été enregistré",
        ];
        try {
            $data = $request->validated();
            $data['id'] =  Str::uuid();
            $data['administrateur_id'] =  Auth::user()->id;

            /** @var UploadedFile|null $image */
            $image = $request->validated('file');
            if ($image !== null && !$image->getError()) {
                $data['picture'] = $image->store('profils', 'public');
            }
            $response ['datas'] = Profil::create($data);
        } catch (\Exception $e) {
            $code = 422;
            $response ['code'] = $code;
            $response ['message'] = "Profil - Erreur lors de l'enregistrement du profil";
        }
        return response()->json($response,$code);
    }

    /**
     * Update Profil
     *
     * @param ProfilRequest $request
     * @param Profil $profil
     * @return JsonResponse
     */
    public function update(ProfilRequest $request, Profil $profil): JsonResponse
    {
        $code = 200;
        $response = [
            'code' => $code,
            'success' => true,
            'message' => "Profil - le profil a bien été modifié",
        ];
        try {
            $data = $request->validated();

            /** @var UploadedFile|null $image */
            $image = $request->validated('file');
            if ($image !== null && !$image->getError()) {
                $data['picture'] = $image->store('profils', 'public');
            }
            $profil->update($data);
            $response ['datas'] = $profil;

        } catch (\Exception $e) {
            $code = 422;
            $response ['code'] = $code;
            $response ['message'] = "Profil - Erreur lors de la modification du profil";
        }
        return response()->json($response,$code);
    }

    /**
     * @param Profil $profil
     * @return JsonResponse
     */
    public function destroy(Profil $profil): JsonResponse
    {
        $code = 200;
        $response = [
            'code' => $code,
            'success' => true,
            'message' => "Profil - le profil a bien été supprimé",
        ];

        try {
            $profil->delete();
            $response ['datas'] = $profil;
        } catch (\Exception $e) {
            $code = 422;
            $response ['code'] = $code;
            $response ['message'] = "Profil - Erreur lors de la suppression du profil";
        }

        return response()->json($response,$code);
    }
}
