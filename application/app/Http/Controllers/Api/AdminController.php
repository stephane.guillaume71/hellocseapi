<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRegister;
use App\Http\Requests\LogAdminRegister;
use App\Models\Administrateur;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AdminController extends Controller
{
    public function register(AdminRegister $request)
    {
        $code = 200;
        $response = [
            'code' => $code,
            'success' => true,
            'message' => "Administateur - l'enregistrement d'un administrateur a bien été enregistré",
        ];
        try {
            $data = $request->validated();
            $data['id'] = Str::uuid();
            $response ['datas'] = Administrateur::create($data);

        } catch (\Exception $e) {
            $code = 422;
            $response ['code'] = $code;
            $response ['message'] = "Administateur - Erreur lors de l'enregistrement de l'administrateur";
        }
        return response()->json($response,$code);
    }

}
