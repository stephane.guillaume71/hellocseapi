<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LogAdminRegister;
use App\Models\Administrateur;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(LogAdminRegister $request): JsonResponse
    {
        $code = 200;
        $response = [
            'code' => $code,
            'success' => true,
            'message' => "Connexion - Administrateur connecté",
        ];

        if (Auth::attempt($request->validated())) {
            $admin = Auth::user();
            $token = $admin->createToken('hello_cse_2024')->plainTextToken;
            $response['admin'] = $admin;
            $response['token'] = $token;
        } else {
            $code = 403;
            $response ['code'] = $code;
            $response ['message'] = "Connexion - Information non valide";
        }
        return response()->json($response,$code);
    }

    public function logout(Request $request): JsonResponse
    {
        $user = $request->user();
        $user->tokens()->delete();
        return response()->json([
            'success' => true,
            'code' => 200,
            'message' => "Connexion - Administrateur deconnecté",
        ]);
    }

}
