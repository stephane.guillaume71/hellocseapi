<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommentaireRequest;
use App\Models\Commentaire;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class CommentaireController extends Controller
{

    public function store(CommentaireRequest $request): JsonResponse
    {
        $code = 200;
        $response = [
            'code' => $code,
            'success' => true,
            'message' => "Commentaire - l'enregistrement du profil a bien été enregistré",
        ];

        try {
            $data = $request->validated();
            if($this->isAloneAdminOneProfil($data['profil_id'], Auth::user()->id)) {
                $response ['message'] = "Commentaire - L'administrateur a déjà déposé un comentaire sur le profil";
                return response()->json($response,$code);
            }
            $data['id'] = Str::uuid();
            $data['administrateur_id'] = Auth::user()->id;
            $response ['datas'] = Commentaire::create($data);
        } catch (\Exception $e) {
            $code = 422;
            $response ['code'] = $code;
            $response ['message'] = "Commentaire - Erreur lors de l'enregistrement du profil";
        }
        return response()->json($response,$code);
    }

    private function isAloneAdminOneProfil(string $profilUuid, string $adminUuid): bool
    {
        $result = false;
        $profilAdminProfil = Commentaire::select('content', 'administrateur_id', 'profil_id')
            ->where('administrateur_id', $adminUuid)
            ->where('profil_id', $profilUuid)
            ->get()->count();

        if ($profilAdminProfil > 0) {
            $result = true;
        }
        return $result;
    }
}
