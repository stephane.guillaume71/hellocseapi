<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProfilRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'firstname' => 'required|string|max:100',
            'lastname' => 'required|string|max:100',
            'file' => 'file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'status' => [
                'required',
                Rule::in(['INACTIF', 'ATTENTE', 'ACTIF'])
            ]
        ];
    }

    /**
     * @param Validator $validator
     * @return mixed
     * @throws HttpResponseException
     */
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'code' => 422,
            'message' => 'Profil - Erreur de validation',
            'datas' => [],
            'errorsList' => $validator->errors()
        ]));
    }

    /**
     * @return string[]
     */
    public function messages()
    {
        return [
            'firstname.required' => 'Le prénom doit être renseigné',
            'lastname.required' => 'Le nom doit être renseigné',
            'file.mines' => 'Le format de fichier doit êtres (jpeg,png,jpg,gif,svg)',
            'status.required' => 'Le statut  doit être renseigné ("ACTIF", "INACTIF", "ATTENTE)',
        ];
    }
}
