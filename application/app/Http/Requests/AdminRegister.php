<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class AdminRegister extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'email' => 'required|unique:administrateurs,email|email',
            'password' => 'required|string',

        ];
    }

    /**
     * @param Validator $validator
     * @return mixed
     * @throws HttpResponseException
     */
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'code' => 422,
            'message' => 'Administateur - Erreur de validation',
            'datas' => [],
            'errorsList' => $validator->errors()
        ]));
    }

    /**
     * @return string[]
     */
    public function messages()
    {
        return [
            'email.required' => 'Un mail doit être renseigné',
            'email.unique' => 'Le mail existe déjà',
            'email.email' => 'Le mail n\'est pas un mail valide',
            'password.required' => 'Le mot de passe doit être renseigné',
        ];
    }
}
