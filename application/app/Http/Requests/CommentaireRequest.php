<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CommentaireRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'content' => 'required',
            'profil_id' => 'required|exists:profils,id',
        ];
    }

    /**
     * @param Validator $validator
     * @return mixed
     * @throws HttpResponseException
     */
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'code' => 422,
            'message' => 'Commentaire - Erreur de validation',
            'datas' => [],
            'errorsList' => $validator->errors()
        ]));
    }

    /**
     * @return string[]
     */
    public function messages()
    {
        return [
            'content.required' => 'Le commentaire doit être renseigné',
            'profil_id.required' => "L'id du profil doit être renseigné",
            'profil_id.exists' => "L'id du profil ne correspond à un profil existant",
        ];
    }
}
