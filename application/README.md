# Hello_cse - Projet GUILLAUME Stéphane

## Lancement du proget

### Lancement des migrations et des seeders
```cmd
docker exec php php artisan migrate:fresh --seed  
```

## Fichier Insomnia

Récupérer le fichier des routes api pour le client HTTP

[Route Api (Insomnia)](../.ressources/Insomnia_2024-04-14.json)

(Dans .ressources/)
```
Insomnia_2024-04-12.json
Insomnia_2024dfsdf-04-12.json
```
![imsomnia](../.ressources/imsomnia.png)

## Les accès API
Les améliorations :
- Mettre swagger en place

### Register 
(Pas demandé mais utile pour les tests)

Méthode POST
```
/api/register/
```

![register](../.ressources/register.png)

### Login

Méthode POST
```
/api/login/
```

les identifiants de login sans register : 

```
"email" : "test@example.com",
"password" : "password"
```

![login](../.ressources/login.png)

### Profil

#### Public

Méthode GET
```
/api/profils/
```
![profil](../.ressources/profil1.png)

#### Priver

Lors de la connexion avec le login un token a été retourner,

il est à utiliser dans le header :
```
key : Autorization 
value Bearer <token>
```

Méthode GET
```
/api/profils/
```
![profil](../.ressources/profil2.png)

### Un seul profil 
(pas demandé)

Lors de la connexion avec le login un token a été retourner,

il est à utiliser dans le header :
```
key : Autorization 
value Bearer <token>
```

Méthode GET
```
/api/profils/<Profil ID>
```
Pour le profil Id peut être récupérer dans la liste des profils avec le token ( route précédente)

![login](../.ressources/profil.png)

### Création d'un profil

Lors de la connexion avec le login un token a été retourner,

il est à utiliser dans le header :
```
key : Autorization 
value Bearer <token>
```

Méthode POST / multipart

```
/api/profils/create
```

![creation de profil 1](../.ressources/create_profil1.png)
![creation de profil 2](../.ressources/create_profil2.png)

Pour vérifier l'upload des fichiers 

```
<base_url>/storage/<image_url>
```

### Modification d'un profil

Lors de la connexion avec le login un token a été retourner,

il est à utiliser dans le header :
```
key : Autorization 
value Bearer <token>
```

Méthode POST / multipart

```
/api/profils/update/<profil id>
```
Pour le profil Id peut être récupérer dans la liste des profils avec le token.

![modification de profil 1](../.ressources/update_profil1.png)
![modification de profil 2](../.ressources/update_profil2.png)

Pour vérifier l'upload des fichiers

```
<base_url>/storage/<image_url>
```

### Suppression d'un profil

Lors de la connexion avec le login un token a été retourner,

il est à utiliser dans le header :
```
key : Autorization 
value Bearer <token>
```

Méthode DELETE

```
/api/profils/delete/<profil id>
```
Pour le profil Id peut être récupérer dans la liste des profils avec le token.

![delete](../.ressources/delete.png)

### Creation d'un commentaire

Lors de la connexion avec le login un token a été retourner,

il est à utiliser dans le header :
```
key : Autorization 
value Bearer <token>
```

Méthode Post

```
/api/commentaires/create
```

Les champs utilisés :

```
"content" : "je suis un commentaire",
"profil_id" : <profil id>
```

![creation commentaire1](../.ressources/commentaire1.png)
![creation commentaire2](../.ressources/commentaire2.png)

## Tests

Pour lancer les tests
```
docker exec php php artisan test   
```
