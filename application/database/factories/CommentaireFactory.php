<?php

namespace database\factories;

use App\Models\Administrateur;
use App\Models\Commentaire;
use App\Models\Profil;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Commentaire>
 */
class CommentaireFactory extends Factory
{
    protected $model = Commentaire::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */ /**
 * Define the model's default state.
 *
 * @return array<string, mixed>
 */
    public function definition(): array
    {
        $administrateursId = Administrateur::all(['id']);
        $profilsId = Profil::all(['id']);
        $now = now();
        return [
            'id' => fake()->uuid(),
            'content' => fake()->password(),
            'profil_id' => $profilsId[rand(1,30)]->getAttributes()['id'],
            'administrateur_id' => $administrateursId[rand(1,10)]->getAttributes()['id'],
            'created_at' => $now,
            'updated_at' => $now
        ];
    }
}
