<?php

namespace database\factories;

use App\Models\Administrateur;
use App\Models\Profil;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Profil>
 */
class ProfilFactory extends Factory
{

    protected $model = Profil::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $administrateursId = Administrateur::all(['id']);
        $arrayStatus = ['INACTIF', 'ACTIF', 'ATTENTE'];
        $now = now();
        return [
            'id' => fake()->uuid(),
            'firstname' => fake()->firstName(),
            'lastname' => fake()->lastName(),
            'picture' => fake()->imageUrl(),
            'status' => fake()->randomElement($arrayStatus),
            'administrateur_id' => $administrateursId[rand(1,10)]->getAttributes()['id'],
            'created_at' => $now,
            'updated_at' => $now
        ];
    }
}
