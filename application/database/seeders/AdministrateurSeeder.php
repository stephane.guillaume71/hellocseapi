<?php

namespace Database\Seeders;

use App\Models\Administrateur;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AdministrateurSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Administrateur::factory()
            ->count(10)
            ->create();

        $now = now();
        DB::table('administrateurs')->insert([
            'id' => fake()->uuid(),
            'email' =>'test@example.com',
            'password' => Hash::make('password'),
            'email_verified_at' => $now,
            'remember_token' => Str::random(10),
            'created_at' => $now,
            'updated_at' => $now
        ]);
    }
}
