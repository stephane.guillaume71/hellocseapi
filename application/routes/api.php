<?php

use App\Http\Controllers\Api\AdminController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CommentaireController;
use App\Http\Controllers\Api\ProfilController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::post('login', [AuthController::class, 'login']);
Route::post('register', [AdminController::class, 'register']);

Route::middleware('auth:sanctum')->group(function(){
    // retourne admin connecté
    Route::get('/user', function(Request $request){
        return $request->user();
    });
    Route::post('logout', [AuthController::class, 'logout']);

    Route::get('profils/all', [ProfilController::class, 'index']);
    Route::get('profils/{profil}', [ProfilController::class, 'show']);
    Route::post('profils/create', [ProfilController::class, 'store']);
    Route::post('profils/update/{profil}', [ProfilController::class, 'update']);
    Route::delete('profils/delete/{profil}', [ProfilController::class, 'destroy']);
    Route::post('commentaires/create', [CommentaireController::class, 'store']);
    Route::put('commentaires/update/{commentaires}', [CommentaireController::class, 'update']);
});

Route::get('profils', [ProfilController::class, 'allActif']);
