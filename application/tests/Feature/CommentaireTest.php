<?php

namespace tests\Feature;

use App\Models\Administrateur;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CommentaireTest extends TestCase
{

    use RefreshDatabase;

    public function test_commentaire_can_store(): void
    {
        /** @var Administrateur $Administrateur */
        $Administrateur = Administrateur::factory()->create();

        $login = [
            "email" => $Administrateur->email,
            "password" => 'password',
        ];
        $response = $this->json('POST', 'api/login', $login);
        $token = $response->json()['token'];
        $profil = $this->withHeaders(['Authorization' => "Bearer $token"])->json('POST', 'api/profils/create', [
            "firstname" => "testdgdfgdsfg",
            "lastname" => "test5dsfgdsfgsdfg",
            "status" => "ACTIF",
        ])
            ->assertStatus(200)
            ->assertJson([
                'code' => 200,
                'success' => true,
                'message' => "Profil - l'enregistrement du profil a bien été enregistré",
            ]);

        $profilId = $profil->json()['datas']['id'];

        $this->withHeaders(['Authorization' => "Bearer $token"])->json('POST', 'api/commentaires/create', [
            "content" => "testdgdfgdsfg sdfhjskdfh sdlhfjskdfhjkqsdc sdjfhlsjdkqf",
            "profil_id" => $profilId,
        ])
            ->assertStatus(200)
            ->assertJson([
                'code' => 200,
                'success' => true,
                'message' => "Commentaire - l'enregistrement du profil a bien été enregistré",
            ]);
    }

    public function test_commentaire_not_can_store_by_idem_admin(): void
    {
        /** @var Administrateur $Administrateur */
        $Administrateur = Administrateur::factory()->create();

        $login = [
            "email" => $Administrateur->email,
            "password" => 'password',
        ];
        $response = $this->json('POST', 'api/login', $login);
        $token = $response->json()['token'];
        $profil = $this->withHeaders(['Authorization' => "Bearer $token"])->json('POST', 'api/profils/create', [
            "firstname" => "testdgdfgdsfg",
            "lastname" => "test5dsfgdsfgsdfg",
            "status" => "ACTIF",
        ])
            ->assertStatus(200)
            ->assertJson([
                'code' => 200,
                'success' => true,
                'message' => "Profil - l'enregistrement du profil a bien été enregistré",
            ]);

        $profilId = $profil->json()['datas']['id'];

        // first commentaire
        $this->withHeaders(['Authorization' => "Bearer $token"])->json('POST', 'api/commentaires/create', [
            "content" => "testdgdfgdsfg sdfhjskdfh sdlhfjskdfhjkqsdc sdjfhlsjdkqf",
            "profil_id" => $profilId,
        ])
            ->assertStatus(200)
            ->assertJson([
                'code' => 200,
                'success' => true,
                'message' => "Commentaire - l'enregistrement du profil a bien été enregistré",
            ]);
        // two commentaire
        $this->withHeaders(['Authorization' => "Bearer $token"])->json('POST', 'api/commentaires/create', [
            "content" => "je suis un contenu",
            "profil_id" => $profilId,
        ])
            ->assertStatus(200)
            ->assertJson([
                'code' => 200,
                'success' => true,
                'message' => "Commentaire - L'administrateur a déjà déposé un comentaire sur le profil",
            ]);
    }
    public function test_commentaire_not_can_store(): void
    {
        /** @var Administrateur $Administrateur */
        $Administrateur = Administrateur::factory()->create();

        $login = [
            "email" => $Administrateur->email,
            "password" => 'password',
        ];
        $response = $this->json('POST', 'api/login', $login);
        $token = $response->json()['token'];
        $profil = $this->withHeaders(['Authorization' => "Bearer $token"])->json('POST', 'api/profils/create', [
            "firstname" => "testdgdfgdsfg",
            "lastname" => "test5dsfgdsfgsdfg",
            "status" => "ACTIF",
        ])
            ->assertStatus(200)
            ->assertJson([
                'code' => 200,
                'success' => true,
                'message' => "Profil - l'enregistrement du profil a bien été enregistré",
            ]);

        $profilId = $profil->json()['datas']['id'];

        $this->withHeaders(['Authorization' => "Bearer $token"])->json('PUT', 'api/commentaires/create', [
            "content" => "testdgdfgdsfg sdfhjskdfh sdlhfjskdfhjkqsdc sdjfhlsjdkqf",
            "profil_id" => $profilId,
        ])
            ->assertStatus(405);
    }
}
