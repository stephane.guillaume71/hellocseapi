<?php

namespace tests\Feature;

use App\Models\Administrateur;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthTest extends TestCase
{

    use RefreshDatabase;

//    public function test_admin_can_not_found_login(): void
//    {
//        $response = $this->get('/login/toto');
//        $response->assertStatus(404);
//    }
    public function test_admin_can_login(): void
    {
        /** @var Administrateur $Administrateur */
        $Administrateur = Administrateur::factory()->create();

        $login = [
            "email" => $Administrateur->email,
            "password" => 'password',

        ];

        $this->json('POST', 'api/login', $login)
            ->assertStatus(200)
            ->assertJson([
                'code' => '200',
                'success' => true,
                'message' => "Connexion - Administrateur connecté",
            ]);

    }

    public function test_admin_cannot_login_with_incorrect_password(): void
    {
        /** @var Administrateur $Administrateur */
        $Administrateur = Administrateur::factory()->create();

        // incorrect password
        $login = [
            "email" => $Administrateur->email,
            "password" => 'invalid-password',

        ];
        $this->json('POST', 'api/login', $login)
            ->assertStatus(403)
            ->assertJson([
                'code' => 403,
                'success' => true,
                'message' => "Connexion - Information non valide",
            ]);

        // missing password
        $login = [
            "email" => $Administrateur->email,
        ];
        $this->json('POST', 'api/login', $login)
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
                'code' => 422,
                'message' => 'Erreur de validation',
                'datas' => []
            ]);
    }

    public function test_admin_cannot_login_with_incorrect_email(): void
    {

        $login = [
            "email" => 'dsqdqsdsd@gmail.com',
            "password" => 'password',

        ];
        $this->json('POST', 'api/login', $login)
            ->assertStatus(200)
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
                'code' => 422,
                'message' => 'Erreur de validation',
                'datas' => []
            ]);

        $login = [
            "password" => 'password',

        ];
        $this->json('POST', 'api/login', $login)
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
                'code' => 422,
                'message' => 'Erreur de validation',
                'datas' => []
            ]);
    }

    public function test_admin_can_logout(): void
    {
        /** @var Administrateur $Administrateur */
        $Administrateur = Administrateur::factory()->create();

        $login = [
            "email" => $Administrateur->email,
            "password" => 'password',

        ];

        $response = $this->json('POST', 'api/login', $login);
        $token = $response->json()['token'];
        $this->withHeaders(['Authorization' => "Bearer $token"])
            ->json('POST', '/api/logout', [
                'title' => 'book post',
                'author' => 'post author'
            ])->assertStatus(200)
            ->assertJson([
                'success' => true,
                'code' => 200,
                'message' => "Connexion - Administrateur deconnecté",
            ]);
    }

}
