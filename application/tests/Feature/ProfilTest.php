<?php

namespace tests\Feature;

use App\Models\Administrateur;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProfilTest extends TestCase
{

    use RefreshDatabase;


    public function test_profil_public_allActif(): void
    {
        $this->json('GET', 'api/profils')
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
                'code' => 200,
                'message' => "Profil - La liste de tous les profils Actif",
            ]);
    }

    public function test_profil_not_public_allActif(): void
    {
        /** @var Administrateur $Administrateur */
        $Administrateur = Administrateur::factory()->create();

        $login = [
            "email" => $Administrateur->email,
            "password" => 'password',

        ];

        $response = $this->json('POST', 'api/login', $login);
        $token = $response->json()['token'];
        $this->withHeaders(['Authorization' => "Bearer $token"])->json('GET', 'api/profils')
            ->assertStatus(302);
    }

    public function test_profil_can_store(): void
    {
        /** @var Administrateur $Administrateur */
        $Administrateur = Administrateur::factory()->create();

        $login = [
            "email" => $Administrateur->email,
            "password" => 'password',
        ];
        $response = $this->json('POST', 'api/login', $login);
        $token = $response->json()['token'];
        $this->withHeaders(['Authorization' => "Bearer $token"])->json('POST', 'api/profils/create', [
            "firstname" => "testdgdfgdsfg",
            "lastname" => "test5dsfgdsfgsdfg",
            "status" => "ACTIF",
        ])
            ->assertStatus(200)
            ->assertJson([
            'code' => 200,
            'success' => true,
            'message' => "Profil - l'enregistrement du profil a bien été enregistré",
        ]);

    }

    public function test_profil_not_can_store(): void
    {
        /** @var Administrateur $Administrateur */
        $Administrateur = Administrateur::factory()->create();

        $login = [
            "email" => $Administrateur->email,
            "password" => 'password',
        ];
        $response = $this->json('POST', 'api/login', $login);
        $token = $response->json()['token'];
        $this->withHeaders(['Authorization' => "Bearer $token"])->json('GET', 'api/profils/create', [
            "firstname" => "testdgdfgdsfg",
            "lastname" => "test5dsfgdsfgsdfg",
            "status" => "ACTIF",
        ])->assertStatus(404);
    }

    public function test_profil_can_update(): void
    {
        /** @var Administrateur $Administrateur */
        $Administrateur = Administrateur::factory()->create();

        $login = [
            "email" => $Administrateur->email,
            "password" => 'password',
        ];
        $response = $this->json('POST', 'api/login', $login);
        $token = $response->json()['token'];
        $profil = $this->withHeaders(['Authorization' => "Bearer $token"])->json('POST', 'api/profils/create', [
            "firstname" => "testdgdfgdsfg",
            "lastname" => "test5dsfgdsfgsdfg",
            "status" => "ACTIF",
        ])
            ->assertStatus(200)
            ->assertJson([
                'code' => 200,
                'success' => true,
                'message' => "Profil - l'enregistrement du profil a bien été enregistré",
            ]);

        $profilId = $profil->json()['datas']['id'];

        $newProfil = $this->withHeaders(['Authorization' => "Bearer $token"])->json('POST', 'api/profils/update/' . $profilId, [
            "firstname" => "sqdsdfssqdfsdfdf",
            "lastname" => "test5dsfgdsfgsdfg",
            "status" => "INACTIF",
        ])
            ->assertStatus(200)
            ->assertJson([
                'code' => 200,
                'success' => true,
                'message' => "Profil - le profil a bien été modifié",
            ]);
    }

    public function test_profil_not_can_update(): void
    {
        /** @var Administrateur $Administrateur */
        $Administrateur = Administrateur::factory()->create();

        $login = [
            "email" => $Administrateur->email,
            "password" => 'password',
        ];
        $response = $this->json('POST', 'api/login', $login);
        $token = $response->json()['token'];
        $newProfil = $this->withHeaders(['Authorization' => "Bearer $token"])->json('POST', 'api/profils/update/sdfdsfsd-sdfd', [
            "firstname" => "sqdsdfssqdfsdfdf",
            "lastname" => "test5dsfgdsfgsdfg",
            "status" => "INACTIF",
        ])
            ->assertStatus(404);
    }

    public function test_profil_can_destroy(): void
    {
        /** @var Administrateur $Administrateur */
        $Administrateur = Administrateur::factory()->create();

        $login = [
            "email" => $Administrateur->email,
            "password" => 'password',
        ];
        $response = $this->json('POST', 'api/login', $login);
        $token = $response->json()['token'];
        $profil = $this->withHeaders(['Authorization' => "Bearer $token"])->json('POST', 'api/profils/create', [
            "firstname" => "testdgdfgdsfg",
            "lastname" => "test5dsfgdsfgsdfg",
            "status" => "ACTIF",
        ])
            ->assertStatus(200)
            ->assertJson([
                'code' => 200,
                'success' => true,
                'message' => "Profil - l'enregistrement du profil a bien été enregistré",
            ]);

        $profilId = $profil->json()['datas']['id'];
        $this->withHeaders(['Authorization' => "Bearer $token"])->json('DELETE', 'api/profils/delete/' . $profilId)
            ->assertStatus(200)
            ->assertJson([
                'code' => 200,
                'success' => true,
                'message' => "Profil - le profil a bien été supprimé",
            ]);
    }

    public function test_profil_not_can_destroy(): void
    {
        /** @var Administrateur $Administrateur */
        $Administrateur = Administrateur::factory()->create();

        $login = [
            "email" => $Administrateur->email,
            "password" => 'password',
        ];
        $response = $this->json('POST', 'api/login', $login);
        $token = $response->json()['token'];
        $profil = $this->withHeaders(['Authorization' => "Bearer $token"])->json('POST', 'api/profils/create', [
            "firstname" => "testdgdfgdsfg",
            "lastname" => "test5dsfgdsfgsdfg",
            "status" => "ACTIF",
        ])
            ->assertStatus(200)
            ->assertJson([
                'code' => 200,
                'success' => true,
                'message' => "Profil - l'enregistrement du profil a bien été enregistré",
            ]);

        $profilId = $profil->json()['datas']['id'];
        $this->withHeaders(['Authorization' => "Bearer $token"])->json('POST', 'api/profils/delete/' . $profilId)
            ->assertStatus(405);
    }
}
