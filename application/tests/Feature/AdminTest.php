<?php

namespace tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AdminTest extends TestCase
{

    use RefreshDatabase;

    public function test_admin_can_register(): void
    {
        $login = [
            "email" => 'test5@test.com',
            "password" => 'password',
        ];

        $this->json('POST', 'api/register', $login)
            ->assertStatus(200)
            ->assertJson([
                'code' => 200,
                'success' => true,
                'message' => "Administateur - l'enregistrement d'un administrateur a bien été enregistré",
            ]);
    }

    public function test_admin_cannot_register_email_exist(): void
    {

        $login = [
            "email" => 'test5@test.com',
            "password" => 'password',
        ];

        $this->json('POST', 'api/register', $login)
            ->assertStatus(200)
            ->assertJson([
                'code' => 200,
                'success' => true,
                'message' => "Administateur - l'enregistrement d'un administrateur a bien été enregistré",
            ]);

        $this->json('POST', 'api/register', $login)
            ->assertStatus(200)
            ->assertJson([
                "success" => false,
                "code"=> 422,
                "message"=> "Administateur - Erreur de validation",
                "datas"=> [],
                "errorsList"=> [
                    "email"=> [
                        "Le mail existe déjà"
                    ]
	            ]
            ]);
    }

    public function test_admin_not_can_register(): void
    {
        $login = [
            "email" => 'test5@test.com',
        ];

        $this->json('POST', 'api/register', $login)
            ->assertStatus(200)
            ->assertJson([
                'code' => 422,
                'success' => false,
                'message' => "Administateur - Erreur de validation",
            ]);
    }
}
