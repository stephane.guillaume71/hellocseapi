# Hello_cse - Projet GUILLAUME Stéphane

##  Avant le lancement du projet

### Organisation et étude

- Dans premier temps j'ai lu plusieurs fois le cahier des charges.
- Dans un second temps j'ai réalisé le découpage en issues. 
- En suite je les ai mit dans mon issue board
- Des commits ont été réaliasé et stash à chaques merge dans develop et main  

#### Les issues
![Les issues](.ressources/issues2.png)

#### Le board
![issues board.png](.ressources/issues_board.png)
![issues board.png](.ressources/issues_board2.png)

#### Conclusion

En tout cas merci je me suis fait plaisir et je me suis bien amusé.

##  1. Intallation de lenvironnement du projet

- Avoir Docker installer

Ce placer à la racine du projet.

Création des containers:

```cmd
	docker compose build --no-cache
	docker compose up --pull always -d --wait
	docker compose down --remove-orphans 
```

Vérifier le fichier env : 

```php

	DB_CONNECTION=mysql
	DB_HOST=db
	DB_PORT=3306
	DB_DATABASE=hellocse
	DB_USERNAME=root
	DB_PASSWORD=root

```

Lancer la commande suivante pour la création :

1er manière : 

Sans rentrer dans le container php 

### Installation des dépendances de laravel

```php
docker exec php composer install
```

### Création de la base de donnée

```php
docker exec php php artisan db:create
	
docker exec php php artisan migrate
```

### Les accès aux commandes artisan

```php
docker exec php php artisan <command>
```

2eme manière possible:

En rentrant dans le container php

```php
docker exec -it php bash 
   
composer install
   
php artisan db:create 
```

[L'utilisation du projet](application/README.md)
